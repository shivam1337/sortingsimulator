void bubbleSort(int a[], int n){
	int i, j;
	for(i = 0; i < n; i++){
		for(j = i; j < n-i-1; j++){
			if(a[j+1] > a[j]){
				int temp = a[j+1];
				a[j+1] = a[j];
				a[j] = temp;
			} 
		}
		printArr();
	}
}
