#include<gtk/gtk.h>

GtkWidget *tf, *cb, *tim;

static void destroy (GtkWidget *window, gpointer data){
	gtk_main_quit();
}
static gboolean delete_event (GtkWidget *window, GdkEvent *event, gpointer data){
	return FALSE;
}
static void on_changed (GtkComboBox *widget, gpointer   user_data){
	GtkComboBox *combo_box = widget;
	if (gtk_combo_box_get_active (combo_box) != 0) {
    	gchar *sort = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT(combo_box));
    	g_free (sort);
  	}
}

/*static void sort(){
	GArray *a;
	a = g_array_new(FALSE, FALSE, sizeof(gint));
	
	gchar *in;
	in = gtk_entry_get_text(GTK_ENTRY(tf));
	
	gchar *arr;
	arr = g_strsplit(in, " ", 0);
	gint i;
	for(i = 0; i < 10; i++){
		a[i] = atoi(arr[i]);
	}
	
	gint act;
	act = gtk_combo_get_active(cbox quality);
}*/
int main(int argc, char *argv[]){
	
	GtkWidget *window, *fixed, *l1, *l2, *l3, *l4, *b1, *b2, *b3, *s1, *s2, *s3, *s4, *s5, *s6, *s7, *s8, *s9, *s0;
	gint i;
	
	gtk_init(&argc, &argv);
	
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	fixed = gtk_fixed_new();
	
	gtk_window_set_title(GTK_WINDOW(window), "Sorting Simulator");
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);
	gtk_widget_set_size_request(window, 520, 600);
	
	const gchar *sort[] = {"Bubble Sort", "Quick sort", "Heap sort", "Insertion sort"};
	cb = gtk_combo_box_text_new();

	for(i = 0; i < G_N_ELEMENTS(sort); i++){
		gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (cb), sort[i]);
	}
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (cb), 0);
	
	g_signal_connect (cb, "changed", G_CALLBACK (on_changed), NULL);	
	
	tim = gtk_combo_box_text_new();
	const gchar *tm[] = {"2", "4", "8", "Auto"};
	for(i = 0; i < G_N_ELEMENTS(tm); i++){
		gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (tim), tm[i]);
	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (tim), 0);
	g_signal_connect (tim, "changed", G_CALLBACK (on_changed), NULL);

	l1 = gtk_label_new("Enter 10 numbers (space separated):");
	l2 = gtk_label_new("Select sorting algorithm:");
	l3 = gtk_label_new("Select time:");
	l4 = gtk_label_new("Sorted numbers are:");
	
	b1 = gtk_button_new_with_label("Output");
	b2 = gtk_button_new_with_label("prev");
	b3 = gtk_button_new_with_label("next");
	
	s1 = gtk_button_new_with_label("1");
	s2 = gtk_button_new_with_label("2");
	s3 = gtk_button_new_with_label("3");
	s4 = gtk_button_new_with_label("4");
	s5 = gtk_button_new_with_label("5");
	s6 = gtk_button_new_with_label("6");
	s7 = gtk_button_new_with_label("7");
	s8 = gtk_button_new_with_label("8");
	s9 = gtk_button_new_with_label("9");
	s0 = gtk_button_new_with_label("10");
	
	tf = gtk_entry_new();
	
	gtk_fixed_put(GTK_FIXED(fixed), l1, 120, 20);
	gtk_fixed_put(GTK_FIXED(fixed), tf, 150, 45);
	gtk_fixed_put(GTK_FIXED(fixed), l2, 150, 80);
	gtk_fixed_put(GTK_FIXED(fixed), cb, 180, 110);
	gtk_fixed_put(GTK_FIXED(fixed), l3, 200, 150);
	gtk_fixed_put(GTK_FIXED(fixed), tim, 200, 180);
	gtk_fixed_put(GTK_FIXED(fixed), b1, 205, 220);
	gtk_fixed_put(GTK_FIXED(fixed), l4, 170, 250);
	gtk_fixed_put(GTK_FIXED(fixed), s1, 100, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s2, 130, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s3, 160, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s4, 190, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s5, 220, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s6, 250, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s7, 280, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s8, 310, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s9, 340, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s0, 370, 280);
	gtk_fixed_put(GTK_FIXED(fixed), b2, 100, 320);
	gtk_fixed_put(GTK_FIXED(fixed), b3, 355, 320);
	
	g_signal_connect(G_OBJECT(b1), "sort", G_CALLBACK(sort), NULL);
	
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(destroy), NULL);
	g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), NULL);
	
	//g_signal_connect_swapped (G_OBJECT (button), "clicked", G_CALLBACK (gtk_widget_destroy), (gpointer) window);
	
	gtk_container_add(GTK_CONTAINER(window), fixed);
	gtk_widget_show_all(window);
	
	gtk_main();
	
	return 0;
}
