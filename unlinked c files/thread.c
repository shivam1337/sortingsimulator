#include<pthread.h>
#include<stdio.h>
#include<unistd.h>

void *print(void *vargp){
	int time = (int) vargp;
	sleep(time);
	printf("hello:");
	return NULL;
}

int main(){
	pthread_t t;
	int time;
	scanf("%d", &time);
	pthread_create(&t, NULL, print, (void *)time);
	pthread_join(t, NULL);
	return 0;
}
