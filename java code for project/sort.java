import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

class project extends Thread implements ActionListener{
	JFrame jf;
	JTextArea ta;
	JTextField tf;
	JLabel l1, l2, l3;
	JButton b1, b2, b3;
	JComboBox jcb;
	
	int i, j, temp, n;
	int arr[]=new int[5];
	
	String[] in = new String[5];
	String str;
	String algorithm[] = {"Bubble Sort", "Quick Sort", "Selection Sort", "Heap Sort", "Insertion Sort"};
	
	project(){
		jf = new JFrame("Sorting Simulator");
		ta = new JTextArea("");
		tf = new JTextField();
		l1 = new JLabel("Enter 5 numbers(comma separated):");
		l2 = new JLabel("Select sorting algorithm:");
		l3 = new JLabel("Sorted numbers are:");
		b1 = new JButton("Previous");
		b2 = new JButton("Next");
		b3 = new JButton("Output");
		
		jcb = new JComboBox(algorithm);
		
		l1.setBounds(20, 20, 250, 30);
		tf.setBounds(20, 60, 350, 30);
		l2.setBounds(20, 100, 250, 30);
		jcb.setBounds(20, 140, 150, 30);
		b3.setBounds(120, 220, 140, 40);
		l3.setBounds(20, 260, 250, 30);
		ta.setBounds(20, 300, 350, 30);
		b1.setBounds(20, 340, 100, 30);
		b2.setBounds(270, 340, 100, 30);
		
		ta.setEditable(false);
		
		jf.add(l1);
		jf.add(tf);
		jf.add(b3);
		jf.add(l2);
		jf.add(jcb);
		jf.add(l3);
		jf.add(ta);
		jf.add(b1);
		jf.add(b2);
		
		jf.setLayout(null);
		jf.setSize(405, 500);
		jf.setVisible(true);
		jf.setResizable(false);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
		jcb.addActionListener(this);
	}
	
	Runnable sort = new Runnable(){
		public void run(){
			ta.setText("");
			str = tf.getText();
			in = str.split(" ");
			for(i = 0; i < 5; i++){
				arr[i] = Integer.parseInt(in[i]);
			}
			n = arr.length;
			String s = (String) jcb.getSelectedItem();
			if(s.equals(algorithm[0])){
				bubbleSort(arr);
			}else if(s.equals(algorithm[1])){
				quickSort(arr, 0, n-1);
			}else if(s.equals(algorithm[2])){
				selectionSort(arr);
			}else if(s.equals(algorithm[3])){
				heapSort(arr);
			}else if(s.equals(algorithm[4])){
			insertinSort(arr);
			}
			printarr(arr);
		}
	};
	void bubbleSort(int arr[]){
		n = arr.length;
		for(i = 0; i < n; i++){
			for(j = 1; j < n-i; j++){
				if(arr[j-1] > arr [j]){
					int temp = arr[j-1];
					arr[j-1] = arr[j];
					arr[j] = temp;
				}
				//for(int k=0;k<n;k++)
					//System.out.println(""+arr[k]);
			}
		}
	}
	int partition(int arr[], int low, int high){
        int pivot = arr[high]; 
        i = (low-1);
        for (j=low; j<high; j++){
            if (arr[j] <= pivot){
                i++;
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
        int temp = arr[i+1];
        arr[i+1] = arr[high];
        arr[high] = temp;
 
        return i+1;
    }
	void quickSort(int arr[], int low, int high){
        if (low < high){
            int pi = partition(arr, low, high);
            quickSort(arr, low, pi-1);
            quickSort(arr, pi+1, high);
        }
    }
	
	public void heapSort(int arr[]){
        n = arr.length;
        for (i = n / 2 - 1; i >= 0; i--)
            heapify(arr, n, i);
        for (i=n-1; i>=0; i--){
            temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, i, 0);
        }
    }
	void heapify(int arr[], int n, int i)
    {
        int largest = i;
        int l = 2*i + 1;
        int r = 2*i + 2;
        if (l < n && arr[l] > arr[largest])
            largest = l;
        if (r < n && arr[r] > arr[largest])
            largest = r;
        if (largest != i)
        {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            heapify(arr, n, largest);
        }
    }
	void insertinSort(int arr[]){
        n = arr.length;
        for (i=1; i<n; ++i){
            int key = arr[i];
            j = i-1;
            while (j>=0 && arr[j] > key){
                arr[j+1] = arr[j];
                j = j-1;
            }
            arr[j+1] = key;
        }
    }
	
	void selectionSort(int arr[]){
        n = arr.length;
        for (i = 0; i < n-1; i++)
        {
            int min_idx = i;
            for (j = i+1; j < n; j++)
                if (arr[j] < arr[min_idx])
                    min_idx = j;
            temp = arr[min_idx];
            arr[min_idx] = arr[i];
            arr[i] = temp;
        }
    }
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == b1){
			
		}
		if(e.getSource() == b2){
			
		}
		if(e.getSource() == b3){
			Thread t1 = new Thread(sort);
			t1.start();
		}
	}
	
	void printarr(int arr[]){
		for(i = 0; i < arr.length; i++){
			ta.append(String.valueOf(arr[i]) + " ");
		}
	}
	
	public static void main(String args[]){
		new project();
	}
}		