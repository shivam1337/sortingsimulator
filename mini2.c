#include<gtk/gtk.h>
#include<unistd.h>

GtkWidget *tf, *cb, *s[10]; //*tim,

gchar *srt, *itrTime;

GThread *th;
/*
GMutex mute;
GCond cond; 
*/
//gint thTime, *b;


static void destroy (GtkWidget *window, gpointer data){
	gtk_main_quit();
}

static gboolean delete_event (GtkWidget *window, GdkEvent *event, gpointer data){
	return FALSE;
}

static void on_changed (GtkComboBox *widget, gpointer user_data){
	GtkComboBox *combo_box = widget;
	if (gtk_combo_box_get_active (combo_box) != 0) {
    	srt = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT(combo_box));
  	}
}

/*
static void changed (GtkComboBox *widget, gpointer user_data){
	GtkComboBox *combo_box = widget;
	if (gtk_combo_box_get_active (combo_box) != 0) {
    	itrTime= gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT(combo_box));
  	}
  	if(g_strcmp0(itrTime, "Manual") == 0){
  		
  	}else if(g_strcmp0(itrTime, "8") == 0){
  		thTime = 8;
  	}else if(g_strcmp0(itrTime, "4") == 0){
  		thTime = 4;
  	}else{
  		thTime = 2;
  	}
}
*/
gpointer print(gpointer data){
	gint *a = (gint *)data;
	gint i;
	for(i = 0; i < 10; i++){
		gchar *num;
		num = g_strdup_printf("%d", a[i]);
		gtk_button_set_label(GTK_BUTTON(s[i]), num);
		g_print("%s ", num);
		usleep(100000);
		g_free(num);
		
	}
	g_print("\n");
	return NULL;	
}

 
/*
static void next(){
	//g_cond_signal (&cond);
	//g_mutex_unlock (&mute);
}

static void prev(){
	
}

*/
//QUICK SORT ALGORITHM
void swap(gint* a, gint* b){
    gint t = *a;
    *a = *b;
    *b = t;
}
gint partition (gint arr[], gint low, gint high){
    gint pivot = arr[high];    // pivot
    gint i = (low - 1);  // Index of smaller element

    for (gint j = low; j <= high- 1; j++){
        // If current element is smaller than or
        // equal to pivot
        if (arr[j] <= pivot){
            i++;    // increment index of smaller element
            //g_print("Swapped %d with %d \n", arr[i], arr[j]);
            swap(&arr[i], &arr[j]);
        }
    }
    g_print("Swapped %d with %d \n", arr[i+1], arr[high]);
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(gint arr[], gint low, gint high){
	th = g_thread_new(NULL, print, arr);
	g_thread_join(th);
	g_print("\n");
    if (low < high){
        /* pi is partitioning index, arr[p] is now
           at right place */
        gint pi = partition(arr, low, high);

        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        
        quickSort(arr, pi + 1, high);
    }   
    
}

//BUBBLE SORT ALGORITHM
void bubbleSort(gint arr[], gint n){
	gint i = 0, j;
	//gint k = 1;
	for(i = 0; i < n; i++){
		for(j = 0; j < n - 1 - i; j++){	
			if(arr[j] > arr[j+1]){
				g_print("Swapped %d with %d \n", arr[j], arr[j+1]);
				swap(&arr[j], &arr[j+1]);
			} 	
		}
		th = g_thread_new(NULL, print, arr);
		g_thread_join(th);
		g_print("\n");
	}
	
	//print(arr, n);
}

//HEAP SORT ALGORITHM
void heapify(gint arr[], gint n, gint i){
    gint largest = i;  // Initialize largest as root
    gint l = 2*i + 1;  // left = 2*i + 1
    gint r = 2*i + 2;  // right = 2*i + 2
 
    // If left child is larger than root
    if (l < n && arr[l] > arr[largest])
        largest = l;
 
    // If right child is larger than largest so far
    if (r < n && arr[r] > arr[largest])
        largest = r;
 
    // If largest is not root
    if (largest != i){
        swap(&arr[i], &arr[largest]);
 
        // Recursively heapify the affected sub-tree
        heapify(arr, n, largest);
    }
}
 
// main function to do heap sort
void heapSort(gint arr[], gint n){
    // Build heap (rearrange array)
    for (gint i = n / 2 - 1; i >= 0; i--){
        heapify(arr, n, i);
 	}
    // One by one extract an element from heap
    for (gint i=n-1; i>=0; i--){
        // Move current root to end
        g_print("Swapped %d with %d \n", arr[0], arr[i]);
        swap(&arr[0], &arr[i]);
        th = g_thread_new(NULL, print, arr);
		g_thread_join(th);
 		g_print("\n");
        // call max heapify on the reduced heap
        heapify(arr, i, 0);
    }
    
}

//INSERTION SORT ALGORITHM
void insertionSort(gint arr[], gint n){
    gint i, key, j;
    for (i = 1; i < n; i++){
       key = arr[i];
       j = i-1;
 
       /* Move elements of arr[0..i-1], that are
          greater than key, to one position ahead
          of their current position */
       while (j >= 0 && arr[j] > key){
           arr[j+1] = arr[j];
           j = j-1;
       }
       arr[j+1] = key;
       g_print("Placed %d at position a[0]\n", arr[j+1]);
       th = g_thread_new(NULL, print, arr);
	   g_thread_join(th);
	   g_print("\n");
	}
}

static void out(GtkWidget *window, gpointer data){
	const gchar *in;
	in = gtk_entry_get_text(GTK_ENTRY(tf));
	
	gchar **ar = g_strsplit(in, " ", 0);
	
	gint i;
	gint arr[10];
	
	for(i = 0; i < 10; i++){
		arr[i] = (gint) g_ascii_strtod(ar[i], NULL);
	}
	
  	//th = gtk_thread_new(NULL, th_func, GINT_TO_POINTER(time));
  	g_print("Step-wise simulation of sorting in backgound!\n");
  	if(g_strcmp0(srt, "Quick sort") == 0){
  		quickSort(arr, 0, 9);
  	}else if(g_strcmp0(srt, "Insertion sort") == 0){
  		insertionSort(arr, 10);
  	}else if(g_strcmp0(srt, "Heap sort") == 0){
  		heapSort(arr, 10);
  	}else{
  		bubbleSort(arr, 10);
  	}
  	g_free(srt);
}

int main(int argc, char *argv[]){
	
	GtkWidget *window, *fixed, *l1, *l2, *l4, *b1;// l3, *b2, *b3;
	gint i;
	
	gtk_init(&argc, &argv);
	
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	fixed = gtk_fixed_new();
	
	gtk_window_set_title(GTK_WINDOW(window), "Sorting Simulator");
	gtk_container_set_border_width(GTK_CONTAINER(window), 10);
	gtk_widget_set_size_request(window, 530, 600);
	
	const gchar *sort[] = {"Bubble Sort", "Quick sort", "Heap sort", "Insertion sort"};
	cb = gtk_combo_box_text_new();

	for(i = 0; i < G_N_ELEMENTS(sort); i++){
		gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (cb), sort[i]);
	}
	
	gtk_combo_box_set_active (GTK_COMBO_BOX (cb), 0);
	
	g_signal_connect (cb, "changed", G_CALLBACK (on_changed), NULL);	
	
	/*tim = gtk_combo_box_text_new();
	const gchar *tm[] = {"2", "4", "8", "Manual"};
	for(i = 0; i < G_N_ELEMENTS(tm); i++){
		gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (tim), tm[i]);
	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (tim), 0);
	
	g_signal_connect (tim, "changed", G_CALLBACK (changed), NULL);*/

	l1 = gtk_label_new("Enter 10 numbers (space separated):");
	l2 = gtk_label_new("Select sorting algorithm:");
	//l3 = gtk_label_new("Select time:");
	l4 = gtk_label_new("Sorted numbers are:");
	
	b1 = gtk_button_new_with_label("Output");
	//b2 = gtk_button_new_with_label("prev");
	//b3 = gtk_button_new_with_label("next");
	
	for(i = 0; i < 10; i++){
		s[i] = gtk_button_new_with_label("-");
	}
	
	tf = gtk_entry_new();
	
	gtk_fixed_put(GTK_FIXED(fixed), l1, 120, 20);
	gtk_fixed_put(GTK_FIXED(fixed), tf, 165, 45);
	gtk_fixed_put(GTK_FIXED(fixed), l2, 160, 80);
	gtk_fixed_put(GTK_FIXED(fixed), cb, 180, 110);
	//gtk_fixed_put(GTK_FIXED(fixed), l3, 200, 150);
	//gtk_fixed_put(GTK_FIXED(fixed), tim, 200, 180);
	gtk_fixed_put(GTK_FIXED(fixed), b1, 205, 220);
	gtk_fixed_put(GTK_FIXED(fixed), l4, 170, 250);
	gtk_fixed_put(GTK_FIXED(fixed), s[0], 20, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[1], 70, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[2], 120, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[3], 170, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[4], 220, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[5], 270, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[6], 320, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[7], 370, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[8], 420, 280);
	gtk_fixed_put(GTK_FIXED(fixed), s[9], 470, 280);
	//gtk_fixed_put(GTK_FIXED(fixed), b2, 100, 350);
	//gtk_fixed_put(GTK_FIXED(fixed), b3, 355, 350);
	
	g_signal_connect(b1, "clicked", G_CALLBACK(out), NULL);
	//g_signal_connect(b2, "clicked", G_CALLBACK(prev), NULL);
	//g_signal_connect(b3, "clicked", G_CALLBACK(next), NULL);
	
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(destroy), NULL);
	g_signal_connect(G_OBJECT(window), "delete_event", G_CALLBACK(delete_event), NULL);
	
	//g_signal_connect_swapped (G_OBJECT (button), "clicked", G_CALLBACK (gtk_widget_destroy), (gpointer) window);
	
	gtk_container_add(GTK_CONTAINER(window), fixed);
	gtk_widget_show_all(window);
	
	gtk_main();
	
	return 0;
}
