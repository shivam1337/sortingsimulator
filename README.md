Project title: Simulation of Sorting Algorithms

Name: Shivam Poonamchand Marmat

MIS: 111608072

Project description:

    This project is about simulation of 4 sorting algorithms named as:
        1. Bubble Sort
        2. Quick Sort
        3. Heap Sort
        4. Insertion Sort
        
    Libraries used:
        1. GTK+ 2.0
        2. unistd.h (for usleep() function)
        
    Standard references:
        https://developer.gnome.org/gtk-tutorial/stable/
        Apress.Foundations.of.GTK.plus.Development.Apr.2007.pdf
    
    This software takes a set of 10 numbers as input and gives the sorted
    sequence of numbers as output on gui. Meanwhile, all the background
    processes required for the sorting are represented in the terminal.
    
    In this way, each of the sorting algorithm is simulated for given set
    of inputs.
    